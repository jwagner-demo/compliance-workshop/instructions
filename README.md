# instructions

This is a self-paced workshop for common compliance controls. Once you have imported the group, follow the instructions in this document to enforce compliance standards and report on them.

## Enforce Seperation of Duties & Prevent Authors / Committers From Approving Their Own Work

-------------------------------

**Enforce It**

1. From your top level namespace, click *Settings* in the left navigation menu and then click *General*
2. Scroll down to *Merge request approvals* and click *Expand*
3. Check the boxes for "Prevent approval by author", "Prevent approvals by user who add commits", and "Prevent editing approval rules in projects and merge requests"
4. Click *Save changes*

**Report On It**

1. From the top level namespace, click *Secure* in the left navigation menu and then click *Compliance center*
2. Notice the *node* project has a success next to the check "Prevent committers as approvers"
3. Notice the *node* project has a success next to "Prevent authors as approvers"
4. In the top right, click *Export* and click *Export standards adherence report*
5. You will receive an email with a csv of the report.

**Documentation**

[Seperation of Duties](https://docs.gitlab.com/ee/user/compliance/compliance_center/index.html#separation-of-duties)
[Standards Reporting](https://docs.gitlab.com/ee/user/compliance/compliance_center/index.html#gitlab-standard)

## Enforce Approval For Production Deploys

--------------------------

**Enforce It**

1. From the top level namespace, click the *Engineering* group to expand a list of projects.
2. Click the *node* project.
3. In the left navigation menu, click *Settings* and then click *Merge requests*.
4. Scroll down to *Merge request approvals*
5. Click *Add approval rule*
6. Enter a rule name such as "Tech", update "Approvals required" to 2, and then add 2 users or groups as approvers.
7. Click "Add approval rule"
8. Click *Add approval rule*
9. Enter a rule name such as "CAB Approval", update "Approvals required" to 1, and then add 1 user or group as approvers.


**Report On It**

1. From the top level namespace, click *Secure* in the left navigation menu and then click *Compliance center*
2. Notice the *node* project has a success next to "Prevent authors as approvers"
3. In the top right, click *Export* and click *Export standards adherence report*
4. You will receive an email with a csv of the report.

**Documentation**

[Merge Request Approval Rules](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)
[Standards Reporting](https://docs.gitlab.com/ee/user/compliance/compliance_center/index.html#gitlab-standard)

## Audit Changes On The .gitlab-ci.yml File

--------------------------

1. From the top level namespace, click the *Engineering* group to expand a list of projects.
2. Click the *node* project.
3. Click the file ".gitlab-ci.yml"
4. On the top right, click *History*.
5. Audit the changes.

**Documentation**

[Git File History](https://docs.gitlab.com/ee/user/project/repository/git_history.html)
[Change of Custody Report](https://docs.gitlab.com/ee/user/compliance/compliance_center/#generate-chain-of-custody-report)
[Code Owners](https://docs.gitlab.com/ee/user/project/codeowners/)


## Audit Changes To The Approval Workflow

--------------------------

1. From the top level namespace, click *Secure* in the left navigation menu and then click *Audit events*
2. Notice all of the events tracked for the approval workflow.

**Documentation**

[Compliance management audit events](https://docs.gitlab.com/ee/administration/audit_event_streaming/audit_event_types.html#compliance-management)

## Access Review

--------------------------

1. Follow the instructions for the "GitLab Group Member Report Tool" below. 

**Documentation**

[GitLab Group Member Report Tool](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/gitlab-group-member-report)
